import { navbar } from "vuepress-theme-hope";

export const zhNavbar = navbar([
  "/",
  {
    text: "介绍",
    icon: "creative",
    link: "/info",
    activeMatch: "^/info",
  },
  {
    text: "下载",
    icon: "creative",
    link: "/use/download_link",
    activeMatch: "^/use",
  },
  {
    text: "功能",
    icon: "creative",
    link: "/guide/",
    activeMatch: "^/guide",
  },
  {
    text: "反馈",
    icon: "creative",
    link: "/check_problem",
    activeMatch: "^/check_problem",
  },
  {
    text: "捐赠",
    icon: "creative",
    link: "/donate",
    activeMatch: "^/donate",
  },
  {
    text: "链接",
    icon: "creative",
    link: "/link",
    activeMatch: "^/link",
  }
]);
