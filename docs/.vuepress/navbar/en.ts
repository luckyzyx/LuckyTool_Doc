import { navbar } from "vuepress-theme-hope";

export const enNavbar = navbar([
  "/en/",
  {
      text: "Introduction",
      icon: "creative",
      link: "/en/info",
      activeMatch: "^/en/info",
    },
    {
      text: "Download",
      icon: "creative",
      link: "/en/use/download_link",
      activeMatch: "^/en/use",
    },
    {
      text: "Function",
      icon: "creative",
      link: "/en/guide/",
      activeMatch: "^/en/guide",
    },
    {
      text: "Feedback",
      icon: "creative",
      link: "/en/check_problem",
      activeMatch: "^/en/check_problem",
    },
    {
      text: "Donate",
      icon: "creative",
      link: "/en/donate",
      activeMatch: "^/en/donate",
    },
    {
      text: "Links",
      icon: "creative",
      link: "/link",
      activeMatch: "^/en//link",
    },
]);
