---
# 这是文章的标题
title: PROBLEM CHECK
# 这是页面的图标
icon: page
# 这是侧边栏的顺序
order: 1
# 设置作者
# author: 忆清鸣、luckyzyx
# 设置写作时间
# date: 2020-01-01
# 一个页面可以有多个分类
category:
  - 问题反馈
# 一个页面可以有多个标签
tag:
  - 问题反馈
# 此页面会在文章列表置顶
sticky: false
# 此页面会出现在文章收藏中
star: false
# 你可以自定义页脚
# footer: 页脚
# 你可以自定义版权信息
# copyright: 无版权
---

## Note

### Feedback questions must provide the following documents or information.

- **Screenshot of module homepage information (required)**
- **Module log page error file (required)**
- **LSPosed log file (this item must be provided when there is no module error)**
- **Screen recording and screenshots of problem recurrence (optional)**
- **Scope APK file (required)**

### If you don't give empty feedback according to the rules, you will only get a warning or a ban

## Check the logs

- If you find errors in the module log page, click the Save or Share button in the upper right corner to send the log file to [TG Discussion Group](https://t.me/+F42pfv-c0h4zNDc9).

- If there are no errors, place the module in the background, follow the process described below to reproduce the problem and immediately open the module log page to refresh, and then save the log in time.

- -If there is still no log, you need to open LSPosed, switch to the log page to save the log file and send it to [TG Discussion Group](https://t.me/+F42pfv-c0h4zNDc9).

## Reproduce the process

Module does not take effect and other issues, first determine the reason, and then feedback, you will not use and module is not adapted to be clear!

- 1. Check whether you have installed the latest version of the module

- 2. If you are in a TG group, install the latest beta version of the group file, maybe the problem has been fixed.

- 2. Check if the recommended scopes are checked.

- 3. Check whether the switch of the corresponding function has been turned on.

- 4. Click the Restart button on the top right corner of the module homepage, and select Restart Scope function.

- 5. At this time, the module will automatically stop the scoping and reload the adaptation information of the function at the same time.

- 6. Open the corresponding scope and trigger the function, then it should take effect normally or print the error log.

- 7. If it still doesn't take effect, you can try the article 8.

- 8. Restart the system (functions scoped to the system framework must use this method).

## Extract scopes

- Extract the scope APK file according to the icon or title on the error log.

- (Extraction tool: MT manager or other applications that can obtain the application APK installation package file.)

- If the Android system framework reports an error, open the root directory /system/framework/ folder, find the following files and copy them out

- [ framework.jar ] [ oplus_framework.jar ] [ services.jar ] [ oplus_services.jar ] [ oplus-service-jobscheduler.jar ] [ oplus-service-jobscheduler.jar ], Send to [TG Discussion Group](https://t.me/+F42pfv-c0h4zNDc9)
