---
# 这是文章的标题
title: 下载方式
# 这是页面的图标
icon: page
# 这是侧边栏的顺序
order: 1
# 设置作者
# author: 忆清鸣、luckyzyx
# 设置写作时间
# date: 2020-01-01
# 一个页面可以有多个分类
category:
  - 快速入门
# 一个页面可以有多个标签
tag:
  - 快速入门
# 此页面会在文章列表置顶
sticky: false
# 此页面会出现在文章收藏中
star: false
# 你可以自定义页脚
# footer: 页脚
# 你可以自定义版权信息
# copyright: 无版权
---

禁止引流、搬运、转载、售卖、分享、分流

提倡授之以渔，不是授之以鱼

[GITHUB 页面](https://github.com/Xposed-Modules-Repo/com.luckyzyx.luckytool/releases/tag/18287-1.2.8)

[GITHUB 下载](https://github.com/Xposed-Modules-Repo/com.luckyzyx.luckytool/releases/download/18287-1.2.8/LuckyTool_v1.2.8.18287.apk)

国内用户优先使用以下链接

<!-- https://mirror.ghproxy.com/ -->

[ghproxy mirror CDN](https://mirror.ghproxy.com/https://github.com/Xposed-Modules-Repo/com.luckyzyx.luckytool/releases/download/18287-1.2.8/LuckyTool_v1.2.8.18287.apk)

<!-- https://ghproxy.net/ -->

[ghproxy CDN](https://ghproxy.cn/https://github.com/Xposed-Modules-Repo/com.luckyzyx.luckytool/releases/download/18287-1.2.8/LuckyTool_v1.2.8.18287.apk)

<!-- https://cors.isteed.cc/ -->

[Lufs CDN](https://cors.isteed.cc/https://github.com/Xposed-Modules-Repo/com.luckyzyx.luckytool/releases/download/18287-1.2.8/LuckyTool_v1.2.8.18287.apk)

不要轻信其他平台以及用户的分享，出现问题不予处理，看到请点举报

强烈建议在版本更新后重启一次系统，这将会避免一些系统功能无法生效的问题
